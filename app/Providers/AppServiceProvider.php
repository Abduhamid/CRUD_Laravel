<?php

namespace App\Providers;

use App\Repositories\Card\CardEloquentRepository;
use App\Repositories\Card\CardRepositoryContract;
use App\Repositories\Category\CategoryEloquentRepository;
use App\Repositories\Category\CategoryRepositoryContract;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindRepositories();
//        $this->app->bind(CardRepositoryContract::class, CardEloquentRepository::class);
//        $this->app->bind(CategoryRepositoryContract::class, CategoryEloquentRepository::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    public function registerBindRepositories()
    {
        $this->app->bind(CardRepositoryContract::class, CardEloquentRepository::class);
        $this->app->bind(CategoryRepositoryContract::class, CategoryEloquentRepository::class);
    }
}
