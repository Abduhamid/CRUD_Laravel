<?php

namespace App\Listeners\History;

use App\Models\History\History;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;

class HistoryListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $user_id=Auth::user()->id;
        $history=new History();
        $history->old=json_encode($event->entity->getOldAttributes());
        $history->new=json_encode($event->entity->getChanges());
        $history->user_id=$user_id;
        $history->type=get_class($event->entity);
        $history->model_id=$event->entity->id;
        $history->save();
    }
}
