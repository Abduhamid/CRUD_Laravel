<?php

namespace App\Console\Commands;

use App\Models\Card\Card;
use Illuminate\Console\Command;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ParseExcelCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export:card';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        Card::with('category')
            ->each(function ($card) use ($sheet){
                $i=$card->id+1;
                $sheet->setCellValue('A'.$i, $card->id);
                $sheet->setCellValue('B'.$i, $card->name);
                $sheet->setCellValue('C'.$i, $card->category->name);
                $sheet->setCellValue('D'.$i, $card->pan);
                $sheet->setCellValue('E'.$i, $card->price);
                $sheet->setCellValue('F'.$i, $card->year);
                $sheet->setCellValue('G'.$i, trans('InterfaceTranses.status.'.$card->is_active));
            }, 1000);
        $i=1;
        $sheet->setCellValue('A'.$i, '#');
        $sheet->setCellValue('B'.$i, 'Name');
        $sheet->setCellValue('C'.$i, 'Category');
        $sheet->setCellValue('D'.$i, 'PAN');
        $sheet->setCellValue('E'.$i, 'Price');
        $sheet->setCellValue('F'.$i, 'Date');
        $sheet->setCellValue('G'.$i, 'Status');
        $writer=new Xlsx($spreadsheet);
        $writer->save('Export.xlsx');
    }
}
