<?php

namespace App\Console\Commands;

use Faker\Generator;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\Calculation\Category;
use Psy\Util\Str;
use Faker\Factory;

class SetUsersCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'factory:card';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        DB::table('card')->truncate();
        DB::table('category')->truncate();
        $category=[];
        for ($i=0; $i<100; $i++){
            $faker = Factory::create();
            $category[]=[
                'name'=>$faker->creditCardType,
                'is_active'=>$faker->boolean,
            ];
        }
        DB::table('category')->insert($category);
        for ($i=0; $i<1000; $i++){
            $card=[];
            for ($j=0; $j<100; $j++){
                $faker = Factory::create();
                $card[]=[
                    'name'=>$faker->lastName.' '.$faker->firstName,
                    'pan'=>$faker->creditCardNumber,
                    'price'=>$faker->randomFloat(1,0,10000),
                    'year'=>$faker->date(),
                    'category_id'=>rand(1,100),
                    'is_active'=>$faker->boolean,
                ];
            }
            DB::table('card')->insert($card);
        }
    }
}
