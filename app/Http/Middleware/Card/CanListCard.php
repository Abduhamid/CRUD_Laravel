<?php

namespace App\Http\Middleware\Card;

use Closure;
use Illuminate\Support\Facades\Auth;

class CanListCard
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::user()->ability('admin','card-list')) {
            abort(403);
        }
        return $next($request);
    }
}
