<?php

namespace App\Http\Middleware\Category;

use Closure;
use Illuminate\Support\Facades\Auth;

class CanListCategory
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::user()->ability('admin','category-list')) {
            abort(403);
        }
        return $next($request);
    }
}
