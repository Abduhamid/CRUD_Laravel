<?php

namespace App\Http\Controllers\Category;

use App\Events\History\HistoryEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\Category\StoreCategoryRequest;
use App\Http\Requests\Category\UpdateCategoryRequest;
use App\Repositories\Category\CategoryRepositoryContract;

class CategoryController extends Controller
{
    private $categoryRepositoryContract;

    public function __construct(CategoryRepositoryContract $categoryRepositoryContract)
    {
        $this->categoryRepositoryContract=$categoryRepositoryContract;
        $this->middleware('category.can-list', ['only'=>['index']]);
        $this->middleware('category.can-show', ['only'=>['show']]);
        $this->middleware('category.can-create', ['only'=>['create', 'store']]);
        $this->middleware('category.can-edit', ['only'=>['update', 'edit']]);
        $this->middleware('category.can-delete', ['only'=>['destroy']]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories=$this->categoryRepositoryContract->all('');
        return view('category.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCategoryRequest $request)
    {
        $category=$this->categoryRepositoryContract->create($request->validated());
        event(new HistoryEvent($category));
        session()->flash('flash_message', trans('alerts.general.success_add'));
        return redirect()->route('category.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category=$this->categoryRepositoryContract->findById($id);
        return view('category.show', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category=$this->categoryRepositoryContract->findById($id);
        return view('category.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCategoryRequest $request, $id)
    {
        $category=$this->categoryRepositoryContract->update($request->validated(), $id);
        event(new HistoryEvent($category));
        session()->flash('flash_message', trans('alerts.general.success_edit'));
        return redirect()->route('category.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $category=$this->categoryRepositoryContract->destroy($id);
            event(new HistoryEvent($category));
            session()->flash('flash_message', trans('alerts.general.success_delete'));
            return redirect()->route('category.index');
        }catch (\Exception $ex){
            session()->flash('flash_message_error', trans('errorMessages.errors.try_errors.23000'));
        }
    }
}
