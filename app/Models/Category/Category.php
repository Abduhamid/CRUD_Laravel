<?php

namespace App\Models\Category;

use App\Models\BaseModel;
use App\Models\Card\Card;

class Category extends BaseModel
{
    protected $table='category';
    protected $primaryKey='id';
    protected $fillable=[
        'name',
        'is_active',
    ];

    public function card()
    {
        return $this->hasMany(Card::class, 'category_id');
    }
}
