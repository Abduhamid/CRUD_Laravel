<?php


namespace App\Repositories\Card;

use App\Models\Card\Card;

class CardEloquentRepository implements CardRepositoryContract
{
    protected  $card;

    public function __construct(Card $card)
    {
        $this->card=$card;
    }

    public function all($search)
    {
        return $this->card->where('name', 'like', "%{$search}%")->get();
    }

    public function findById($id)
    {
        return $this->card->where('id', $id)->first();
    }

    public function create($data)
    {
        $card=$this->card->create($data);
        $card->setChanges($card->getAttributes());
        return $card;
    }

    public function update($data, $id)
    {
        $card=$this->card->findOrFail($id);
        $card->setOldAttributes($card->getAttributes());
        $card->update($data);
        return $card;
    }

    public function destroy($id)
    {
        $card=$this->card->findOrFail($id);
        $card->setOldAttributes($card->getAttributes());
        $card->is_active=0;
        $card->save();
        return $card;
    }
}