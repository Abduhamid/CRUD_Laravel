<?php


namespace App\Repositories\Card;


interface CardRepositoryContract
{
    public function all($search);

    public function findById($id);

    public function create($data);

    public function update($data, $id);

    public function destroy($id);
}