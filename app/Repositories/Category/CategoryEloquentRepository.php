<?php


namespace App\Repositories\Category;

use App\Models\Category\Category;

class CategoryEloquentRepository implements CategoryRepositoryContract
{
    private $category;

    public function __construct(Category $category)
    {
        $this->category=$category;
    }

    public function all($search)
    {
        return $this->category->where('name', 'like', "%{$search}%")->get();
    }

    public function findById($id)
    {
        $category=$this->category->where('id', $id)->first();
        $category->setChanges($category->getAttributes());
        return $category;

    }

    public function update($data, $id)
    {
        $category=$this->category->findOrFail($id);
        $category->setOldAttributes($category->getAttributes());
        $category->update($data);
        return $category;
    }

    public function create($data)
    {
        return $this->category->create($data);
    }

    public function destroy($id)
    {
        $category=$this->category->findOrFail($id);
        $category->setOldAttributes($category->getAttributes());
        $category->is_active=0;
        $category->save();
        return $category;
    }
}