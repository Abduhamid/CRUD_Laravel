<?php


namespace App\Repositories\Category;


interface CategoryRepositoryContract
{
    public function all($search);

    public function findById($id);

    public function update($data, $id);

    public function create($data);

    public function destroy($id);
}