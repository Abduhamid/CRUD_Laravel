<?php

Route::group(['namespace' => 'Category'], function(){
    Route::resource('category', 'CategoryController',['except' => ['destroy']]);
    Route::get('category/{id}/delete', ['as' => 'category.delete', 'uses' => 'CategoryController@destroy']);
});