<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
//    \Illuminate\Support\Facades\Log::info('Showing user profile for user: ');
    return view('welcome');
});

Auth::routes();

Route::get('/home', function (){
    return redirect()->route('card.index');
})->name('home');

Route::group(['middleware' => ['auth']], function (){
    require(__DIR__.'/Web/Card.php');
    require(__DIR__.'/Web/Category.php');
});