@foreach($cards as $item)
    {
    recid: '{{$item->id}}',
    name: '{{$item->name}}',
    pan: '{{$item->pan}}',
    price: '{{$item->price}}',
    year: '{{$item->year}}',
    category: '{{$item->category->name}}',
    is_active: '{{trans('InterfaceTranses.is_active.'.$item->is_active)}}',
    updated_at:'{{ $item->updated_at}}',
    created_at:'{{ $item->created_at}}',
    },
@endforeach