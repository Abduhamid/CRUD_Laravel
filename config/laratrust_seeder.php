<?php

return [
    /**
     * Control if the seeder should create a user per role while seeding the data.
     */
    'create_users' => false,

    /**
     * Control if all the laratrust tables should be truncated before running the seeder.
     */
    'truncate_tables' => true,

    'roles_structure' => [
        'admin' => [
            'card' => 'c,r,u,d,l',
            'category' => 'c,r,u,d,l',
        ],
        'moder' => [
            'card' => 'c,l',
        ]
    ],

    'permissions_map' => [
        'c' => 'create',
        'r' => 'show',
        'u' => 'edit',
        'd' => 'delete',
        'l'=>'list'
    ]
];
